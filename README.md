# Restaurant Demo

This demo is created by ReactJS.  


## Installation

Use the package manager [npm](https://docs.npmjs.com/) to install demo.

```bash
npm install 
```

## Plugins
[Framer-motion](https://github.com/framer/motion)  
[React-device-detect](https://github.com/duskload/react-device-detect)  
[React-router-dom](https://github.com/ReactTraining/react-router) 
[Bootstrap 4](https://getbootstrap.com/docs/4.6/getting-started/introduction/)  
[React-bootstrap](https://react-bootstrap.github.io/)   
[Google-map-react](https://github.com/google-map-react/google-map-react)
