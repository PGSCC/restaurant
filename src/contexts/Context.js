/* Button */
export const bookTable = {
    buttonName: "BOOK A TABLE →",
    colorBrown: "#aa7554",
    colorWhite: "#fff"
}

export const checkMenu = {
    buttonName: "CHECK THE MENU →",
    colorWhite: "#fff",
    colorYellow: "#f7b93b",
    colorBlack: "#000"
}

export const subscribe = {
    buttonName: "Subscribe →",
    colorWhite: "#fff",
    colorYellow: "#f7b93b",
    colorBlack: "#000"
}

export const sendMessage = {
    buttonName: "SEND MESSAGE →",
    colorBrown: "#aa7554",
    colorWhite: "#fff"
}

export const reservation = {
    buttonName: "MAKE ME A RESERVATION →",
    colorBrown: "#aa7554",
    colorWhite: "#fff"
}

/* Dish menu */
export const leftEntrees = [
    {
        id: 1,
        foodName: "Spring Rolls",
        option: "CHICKEN OR VEGETABLE",
        price: "$7"
    },
    {
        id: 2,
        foodName: "Fried Won Ton",
        option: "WITH SPICY SOUCE",
        price: "$7"
    },
]

export const rightEntrees = [
    {
        id: 1,
        foodName: "Chichen With Sweet Corn Soup",
        option: "VEGETABLE",
        price: "$8"
    },
    {
        id: 2,
        foodName: "Prawn Crackers",
        option: "WITH SPICY SOUCE",
        price: "$8"
    }
]

export const leftMain = [
    {
        id: 1,
        foodName: "Boneless Lemon Chichen",
        option: "WITH SPECIAL SOUCE",
        price: "$18"
    },
    {
        id: 2,
        foodName: "Garlic Combination",
        option: "WITH CHICKEN AND BEEF",
        price: "$19"
    },
    {
        id: 3,
        foodName: "Beef & Cashew Nuts",
        option: "WITH SPECIAL SOUCE",
        price: "$18"
    },
    {
        id: 4,
        foodName: "Pork Spare Ribs",
        option: "WITH MUSHROOM SOUCE",
        price: "$17"
    }
]

export const rightMain = [
    {
        id: 1,
        foodName: "Prawn Crackers",
        option: "WITH SPICY SOUCE",
        price: "$18"
    },
    {
        id: 2,
        foodName: "Special Fried Rice",
        option: "WITH SPECIAL SOUCE",
        price: "$16"
    },
    {
        id: 3,
        foodName: "Satay Beef",
        option: "WITH SATAY SOUCE",
        price: "$19"
    }
]

export const leftDessert = [
    {
        id: 1,
        foodName: "Ice Cream",
        option: "MANGO OR LYCHEE",
        price: "$8"
    },
    {
        id: 2,
        foodName: "New York Cheesscake",
        option: "TOPPING WITH CARAMEL, CHOCOLATE CURLES AND COCCA POWDER",
        price: "$7"
    }
]

export const rightDessert = [
    {
        id: 1,
        foodName: "Key Lime Pie",
        option: "TANGY CUSTARD WITH GRAHAM CRACKER CRUST",
        price: "$12"
    },
    {
        id: 2,
        foodName: "Mixed Berries",
        option: "FRESH SEASONAL APPLES WITH SPICES AND STREUSEL SPECIAL TOPPING",
        price: "$8"
    }
]


export const special = [
    {
        id: 1,
        foodName: "Beef With Mixed Vegetable",
        option: "WITH SPICY SOUCE",
        price: "$10"
    },
    {
        id: 2,
        foodName: "Beef With Black Bean Souce",
        option: "WITH SPICY SOUCE",
        price: "$10"
    },
    {
        id: 3,
        foodName: "Beef With Cashew Nuts",
        option: "WITH SPICYT SOUCE",
        price: "$10"
    }
]

/* Home Header */
export const home = {
    RestaurantName: "FANTASY RESTAURANT",
    Title: "Pull up a chair. Take a taste. Join us. Life is so endlessly is so endlessly delicious",
    Content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam quam viverra quam efficitur, in pharetra sapien dapibus."
}

/* Other Header */
export const menu = {
    title: "OUR MENU",
    content: "After a good dinner one can forgive anybody, even one's own relations."
}

export const book = {
    title: "BOOK A TABLE",
    content: "Let food be the medicine and medicine be the food."
}

export const contact = {
    title: "Contact Us"
}

/* Discover Page Content */
export const headerContent = {
    RestaurantName: "FANTASY RESTAURANT",
    Title: "Pull up a chair. Take a taste. Join us. Life is so endlessly is so endlessly delicious",
    Content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam quam viverra quam efficitur, in pharetra sapien dapibus."
}

export const specicalContent = {
    Title: "TODAY'S SPECIAL",
    CONTENT: "Enjoy the day's special, straight from the chef to you"
}

/* Disocver Page Promise Content */
export const promisesContent = [
    {
        id: 1,
        imageUrl: "icon_quality.png",
        title: "High Quality",
        content: "We put only the best quality ingriedients in every dish and plate we serve"
    },
    {
        id: 2,
        imageUrl: "icon_royal.png",
        title: "Royal Taste",
        content: "We are dedicated to serve only the finest and tastiest food to our customer"
    },
    {
        id: 3,
        imageUrl: "icon_service.png",
        title: "Quick Service",
        content: "Worry not about waiting too long for your dish, We got that covered for you!"
    }
]

/* Contact Us Form */
export const contactUsFrom = [
    {
        id: 1,
        type: "text",
        placeholder: "NAME"
    },
    {
        id: 2,
        type: "email",
        placeholder: "EMAIL"
    },
    {
        id: 3,
        type: "text",
        placeholder: "SUBJECT"
    }
]

/* Book table form */

export const normalBookTableForm = [
    {
        id: 1,
        type: "text",
        placeholder: "FIRST NAME"
    },
    {
        id: 2,
        type: "text",
        placeholder: "LAST NAME"
    },
    {
        id: 3,
        type: "number",
        placeholder: "PHONE NUMBER"
    }
]

export const optionBookTableForm = [
    {
        id: 1,
        placeholder: "CATEGORY",
        option1: "ROMANTIC BREAKFAST",
        option2: "ROMANTIC BRUNCH",
        option3: "ROMANTIC LUNCH",
        option4: "ROMANTIC AFTERNOON TEA",
        option5: "ROMANTIC DINNER"
    },
    {
        id: 2,
        placeholder: "SEATS",
        option1: "1 GUEST",
        option2: "2 GUESTS",
        option3: "3 GUESTS",
        option4: "5 GUESTS",
        option5: "10 GUESTS"
    },
    {
        id: 3,
        placeholder: "TIME",
        option1: "8:00 AM",
        option2: "10:00 AM",
        option3: "12:00 PM",
        option4: "2:00 PM",
        option5: "6:00 PM"
    }
]