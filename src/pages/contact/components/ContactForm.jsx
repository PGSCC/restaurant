import React from 'react';
import ContactUs from '../../../components/contact/Contact';
import NormalInput from '../../../components/form/Normal';
import Button from '../../../components/button/Button';
import { contactUsFrom, sendMessage } from '../../../contexts/Context';



function ContactForm() {
    function createForm(input) {
        return (
            <NormalInput key={input.id} type={input.type} placeholder={input.placeholder}/>
        );
    }

    return(
        <div className="container ContactForm py-5">
            <div className="row">
                <div className="col-xl-6">
                    <ContactUs />
                </div>
                <div className="col-xl-6">
                    {contactUsFrom.map(createForm)}
                    <textarea className="form-control" rows="3" placeholder="MESSAGE"></textarea>
                    <Button btnBgColor={sendMessage.colorBrown} btnFontColor={sendMessage.colorWhite} btnHoverBgColor={sendMessage.colorWhite} btnHoverFontColor={sendMessage.colorBrown} to="#" btn_name={sendMessage.buttonName} btn_position="btn float-right mt-4"/>
                </div>
            </div>
        </div>
    );
}

export default ContactForm;