import React from 'react';
import './contact.css';
import Header from '../../components/header/OtherHeader';
import ContactForm from './components/ContactForm';
import Map from '../../components/map/Map'
import Footer from '../../components/footer/Footer';
import { contact } from '../../contexts/Context';
import { motion } from 'framer-motion';


function Contact() {
    return(
        <motion.div initial={{ opacity:0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 1 }}>
            <Header title={contact.title} />
            <ContactForm />
            <Map />
            <Footer />
        </motion.div>
    );
}

export default Contact;