import React from 'react';
import './discover.css';
import Header from '../../components/header/Header';
import Special from './components/special/Special';
import Promises from './components/promise/Promises'; 
import Carousel from '../../components/carousel/Carousel';
import Map from '../../components/map/Map'
import Footer from '../../components/footer/Footer';
import { motion } from 'framer-motion';
import { isMobile } from 'react-device-detect';
import { headerContent } from '../../contexts/Context';

function Discover() {
    return (
        <motion.div initial={{ opacity:0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 1 }}>
            <Header RestaurantName={headerContent.RestaurantName} Title={headerContent.Title} Content={headerContent.Content}/>
            <Special />
            <Promises />
            {isMobile && <Carousel />}    
            <Map />
            <Footer />
        </motion.div>

    );
}

export default Discover;