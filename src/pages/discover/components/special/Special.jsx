import React from 'react';
import Image_Dishes from '../../../../images/image_dishes_6.jpeg';
import Dish from '../../../../components/dish/Dish';
import {special, specicalContent} from '../../../../contexts/Context';



function Speical() {

    function CreateSpeicalMenu(special) {
        return(
            <Dish key={special.id} foodName={special.foodName} option={special.option} price={special.price}/>
        );
    }


    return(
        <div className="container">
            <div className="row my-5 Special">
                <div className="col-xl-6 order-1 order-xl-0">
                    <img src={Image_Dishes} alt="dumplings" />
                </div>
                <div className="col-xl-6 order-0 order-xl-1">
                    <h3>{specicalContent.Title}</h3>
                    <h1>{specicalContent.CONTENT}</h1>
                    {special.map(CreateSpeicalMenu)}
                </div>
            </div>
        </div>

    );
}

export default Speical;