import React from 'react';
import Promise from './Single';
import { promisesContent } from '../../../../contexts/Context';


function Promises() {

    function CreatePromise(promise) {
        return(
            <Promise key={promise.id} imageUrl={promise.imageUrl} title={promise.title} content={promise.content} />
        ); 
    }

    return(
        <div className="row Promise text-center py-5 px-5">
            {promisesContent.map(CreatePromise)}
        </div>
    );
}

export default Promises;