import React from 'react';

function SinglePromise(props) {
    return(
        <div className="col-md-4 Single">
            <img src={ require("../../../../images/" + props.imageUrl).default } alt="icon" />
            <h3>{props.title}</h3>
            <p>{props.content}</p>
        </div>
    );
}

export default SinglePromise;