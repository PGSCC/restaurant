import React from 'react';
import './book.css';
import Header from '../../components/header/OtherHeader';
import BookForm from './components/BookForm';
import Map from '../../components/map/Map'
import Footer from '../../components/footer/Footer';
import { book } from '../../contexts/Context';
import { motion } from 'framer-motion';


function Book() {
    return(
        <motion.div initial={{ opacity:0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 1 }}>
            <Header title={book.title} content={book.content}/>
            <BookForm />
            <Map />
            <Footer />
        </motion.div>
    );
}

export default Book;