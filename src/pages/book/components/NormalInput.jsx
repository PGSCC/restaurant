import React from 'react';
import NormalIput from '../../../components/form/Normal';

function Normal(props) {
    return(
        <div className="col-xl-6">
            <NormalIput type={props.type} placeholder={props.placeholder} />
        </div>
        
    );
}

export default Normal;