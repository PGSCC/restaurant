import React from 'react';
import OptionInput from '../../../components/form/Option';
import NormalInput from './NormalInput';
import Button from '../../../components/button/Button';
import { normalBookTableForm ,optionBookTableForm, reservation } from '../../../contexts/Context';

function BookFrom() {
    function createOptionInput(input) {
        return(
            <OptionInput key={input.id} placeholder={input.placeholder} option1={input.option1} option2={input.option2} option3={input.option3} option4={input.option4} option5={input.option5}/>
        );
    }

    function createNormalInput(input) {
        return(
            <NormalInput key={input.id} type={input.type} placeholder={input.placeholder} />
        );
    }

    return(
        <div className="container py-5">
            <div className="row">
                {normalBookTableForm.map(createNormalInput)}
                {optionBookTableForm.map(createOptionInput)}
            </div>
            <div className="text-center">
                <Button btnBgColor={reservation.colorBrown} btnFontColor={reservation.colorWhite} btnHoverBgColor={reservation.colorWhite} btnHoverFontColor={reservation.colorBrown} to="#" btn_name={reservation.buttonName} btn_position="btn mt-4"/>
            </div>
        </div>
    );
}

export default BookFrom;

