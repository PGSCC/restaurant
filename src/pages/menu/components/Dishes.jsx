import React from 'react';
import { leftEntrees, rightEntrees, leftMain, rightMain, leftDessert, rightDessert } from '../../../contexts/Context';
import Dish from '../../../components/dish/Dish';



function Dishes() {

    function CreateDishes(dish) {
        return(
            <Dish key={dish.id} foodName={dish.foodName} option={dish.option} price={dish.price} />
        );
    }

    return(
        <div className="container Dishes">
            <div className="py-5">
                <div className="text-center">
                    <h3>ENTREES</h3>
                </div>
                <div className="row">
                    <div className="col-xl-6">
                        { leftEntrees.map(CreateDishes) }
                    </div>
                    <div className="col-xl-6">
                        { rightEntrees.map(CreateDishes) }  
                    </div>
                </div>
            </div>
            <div className="pb-5">
                <div className="text-center">
                    <h3>MAIN DISHES</h3>
                </div>
                <div className="row">
                    <div className="col-xl-6">
                        { leftMain.map(CreateDishes) }
                    </div>
                    <div className="col-xl-6">
                        { rightMain.map(CreateDishes) }  
                    </div>
                </div>
            </div>
            <div className="pb-5">
                <div className="text-center">
                    <h3>Dessert</h3>
                </div>
                <div className="row">
                    <div className="col-xl-6">
                        { leftDessert.map(CreateDishes) }
                    </div>
                    <div className="col-xl-6">
                        { rightDessert.map(CreateDishes) }  
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Dishes;