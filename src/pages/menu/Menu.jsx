import React from 'react';
import './menu.css';
import { motion } from 'framer-motion';
import Header from '../../components/header/OtherHeader';
import Dishes from './components/Dishes';
import Map from '../../components/map/Map'
import Footer from '../../components/footer/Footer';
import { menu } from '../../contexts/Context';


function Menu() {
    return (
        <motion.div initial={{ opacity:0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 1 }}>
            <Header title={menu.title} content={menu.content}/>
            <Dishes />
            <Map />
            <Footer />
        </motion.div>
    );
}

export default Menu;