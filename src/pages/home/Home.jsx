import React from 'react';
import './home.css';
import showMore from '../../images/icon_showMore.png';
import Button from '../../components/button/Button';
import { checkMenu, home } from '../../contexts/Context';
import { motion } from 'framer-motion';
import { Link } from "react-router-dom";

function Home(){
    return(
        <motion.div initial={{ opacity:0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }} transition={{ duration: 1 }}>
            <div className="Home container">
                <div className="First">
                    <h2>{home.RestaurantName}</h2>
                    <h1>{home.Title}</h1>
                </div>
                <Button btnBgColor={checkMenu.colorWhite} btnFontColor={checkMenu.colorBlack} btnHoverBgColor={checkMenu.colorYellow} btnHoverFontColor={checkMenu.colorWhite} to="menu" btn_name={checkMenu.buttonName} btn_position="btn mt-5"/> 
                <div className="text-center pt-5 showMore">
                    <Link to="discover"><img className="pt-5 mt-5" src={showMore} alt="icon" /></Link>
                </div>
            </div>
        </motion.div>
    );
}

export default Home;