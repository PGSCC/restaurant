import React, { useState } from 'react';
import './form.css';

function OptionForm(props) {

    const [color, setColor] = useState("");

    function changeColor() {
        setColor("#fff");
    }
    
    return(
        <div className="col-xl-6 Form pb-4">
            <select className="form-control" onChange={changeColor} style={{color}} defaultValue={'DEFAULT'}>
                <option value="DEFAULT" disabled>{props.placeholder}</option>
                <option>{props.option1}</option>
                <option>{props.option2}</option>
                <option>{props.option3}</option>
                <option>{props.option4}</option>
                <option>{props.option5}</option>
            </select>
        </div>
    );
}

export default OptionForm;