import React from 'react';
import './form.css';


function NormmalInput(props) {
    return(
        <div className="Form pb-4">
            <input type={props.type} className="form-control" placeholder={props.placeholder} />
        </div>
    );
}

export default NormmalInput;