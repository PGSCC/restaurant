import React from 'react';
import GoogleMapReact from 'google-map-react';
import './map.css';

function Map() {

    const info = {
        center: {
            lat: -37.8136,
            lng: 144.9631
          },
          zoom: 11
    }
    return (
      <div className="Map">
        <GoogleMapReact
            bootstrapURLKeys={{
                key: ['AIzaSyAVXAmx4axn-68RbaEVTVsnieVLFIPOxkA'],
                libraries:['visualization']
            }}  
            defaultCenter={info.center} 
            defaultZoom={info.zoom} >
        </GoogleMapReact>
      </div>
    );
}

export default Map;