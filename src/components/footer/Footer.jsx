import React from 'react';
import './footer.css';
import Button from '../button/Button';
import { subscribe } from '../../contexts/Context';
import { Link } from 'react-router-dom';
import icon_twitter from '../../images/icon_twitter.png';
import icon_facebook from '../../images/icon_facebook.png';
import Contact from '../contact/Contact';


function Footer() {

    return(
        <div className="container Footer pt-5">
            <div className="firstHalf pb-5">
                    <h3>SAVE 20% IN DEALS</h3>
                    <div className="row">
                        <div className="col-xl-6">
                            <h2>Our Weekly Newsletter</h2>
                        </div>
                        <div className="col-xl-6">
                            <div className="row">
                                <div className="col-xl-6">
                                <div className="form-outline">
                                    <input type="email" id="typeEmail" className="form-control" placeholder="TYPE YOUR EMAIL"/>
                                </div>
                                </div>
                                <div className="col-xl-6">
                                    <Button btnBgColor={subscribe.colorWhite} btnFontColor={subscribe.colorBlack} btnHoverBgColor={subscribe.colorYellow} btnHoverFontColor={subscribe.colorWhite} to="/" btn_name={subscribe.buttonName} btn_position="btn"/> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="secondHalf py-5 mb-5">
                <div className="row">
                    <div className="col-xl-4 col-6">
                        <h3>MORE</h3>
                        <p><Link to="#">Our Link</Link></p>
                        <p><Link to="#">Contact Us</Link></p>
                        <p><Link to="#">Delivery Service</Link></p>
                        <p><Link to="#">Terms Of Use</Link></p>
                    </div>
                    <div className="col-xl-4 col-6">
                        <Contact />
                    </div>
                    <div className="col-xl-4">
                        <h3>FOllOW US</h3>
                        <div className="row">
                            <div className="col-2 col-md-1 col-lg-1 col-xl-2">
                                <img src={icon_twitter} alt="icon" />
                            </div>
                            <div className="col-2 col-md-1 col-lg-1 col-xl-2">
                                <img src={icon_facebook} alt="icon" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Footer;