import React from 'react';
import Image_Carousel_1 from '../../images/image_dishes.jpeg';
import Image_Carousel_2 from '../../images/image_dishes_2.jpeg';
import Image_Carousel_3 from '../../images/image_dishes_3.jpeg';
import Image_Carousel_4 from '../../images/image_dishes_4.jpeg';
import Image_Carousel_5 from '../../images/image_dishes_5.jpeg';
import Image_Carousel_6 from '../../images/image_dishes_6.jpeg';
import {Carousel} from 'react-bootstrap';

function CarouselImage() {
    return(
        <Carousel fade>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_1} alt="First slide"/>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_2} alt="First slide"/>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_3} alt="Second slide"/>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_4} alt="Third slide"/>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_5} alt="Forth slide"/>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src={Image_Carousel_6} alt="Forth slide"/>
            </Carousel.Item>
        </Carousel>
    );
}

export default CarouselImage;