import React, {useState} from 'react';
import { Link } from "react-router-dom";

function Button(props) {

    const [isBtnHover, setbtnHover] = useState(false);

    const btn_booktable = {
        backgroundColor: props.btnBgColor,
        color: props.btnFontColor,

    }

    const btn_hover = {
        backgroundColor: props.btnHoverBgColor,
        color: props.btnHoverFontColor
    }

    function btnHandleOver() {
        setbtnHover(true);
    }

    function btnHandleOut() {
        setbtnHover(false);
    }


    return(
        <Link className={props.btn_position} style={isBtnHover ? btn_hover : btn_booktable} to={props.to} onMouseOver={btnHandleOver} onMouseOut={btnHandleOut}>{props.btn_name}</Link>
    );
}

export default Button;