import React from 'react';
import './navbar.css';
import { NavLink } from "react-router-dom";
import Button from '../button/Button';
import { bookTable } from '../../contexts/Context';
import { isDesktop } from 'react-device-detect';

function Navbar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark">
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item"><NavLink className="nav-link" exact to="discover">DISCOVER</NavLink></li>
                    <li className="nav-item"><NavLink className="nav-link" to="menu">OUR MENU</NavLink></li>
                    {isDesktop &&
                        <li className="nav-item"><NavLink className="nav-link" to="gallery">GALLERY</NavLink></li>
                    }
                    <li className="nav-item"><NavLink className="nav-link" to="contact">CONTACT US</NavLink></li>
                </ul>
                <Button btnBgColor={bookTable.colorBrown} btnFontColor={bookTable.colorWhite} btnHoverBgColor={bookTable.colorWhite} btnHoverFontColor={bookTable.colorBrown} to="book_table" btn_name={bookTable.buttonName} btn_position="btn mx-5"/>
            </div>
        </nav>
    );  
}

export default Navbar;