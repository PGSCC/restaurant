import React from 'react';
import './contact.css'


function ContactUs() {
    return(
        <div className="Contact">
            <h3>CONTACT US</h3>
            <p>+614000000</p>
            <p>contact@fantasy.com</p>
            <p>123 Main st Melbourne VIC Australia</p>
        </div>
    );
}

export default ContactUs;