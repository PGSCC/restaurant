import React from 'react';
import './header.css';


function OtherHeader(props) {
    return(
        <div className="OtherHeader py-5">
            <div className="container text-center px-5">
                <h3>{props.title}</h3>
                <h1>{props.content}</h1>
                <h3>~~~</h3>
            </div>
        </div>
    );
}

export default OtherHeader;
