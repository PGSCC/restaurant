import React from 'react';
import { checkMenu } from '../../contexts/Context';
import Button from '../button/Button';
import './header.css';
import Image_Dumplings from '../../images/image_dishes.jpeg';

function DiscoverHeader(props) {
    return(
        <div className="Header my-4 container">
            <div className="row">
                <div className="col-xl-6">
                    <h2>{props.RestaurantName}</h2>
                    <h1>{props.Title}</h1>
                    <h3>~~~</h3>
                    <p>{props.Content}</p>
                    <Button btnBgColor={checkMenu.colorWhite} btnFontColor={checkMenu.colorBlack} btnHoverBgColor={checkMenu.colorYellow} btnHoverFontColor={checkMenu.colorWhite} to="menu" btn_name={checkMenu.buttonName} btn_position="btn mt-2"/>   
                </div>
                <div className="col-xl-6">
                    <img src={Image_Dumplings} alt="dumpling" />
                </div>
            </div> 
        </div>  
    );
}

export default DiscoverHeader;