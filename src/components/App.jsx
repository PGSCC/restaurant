import React from 'react';
import './app.css';
import Navbar from './navbar/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from '../pages/home/Home';
import Discover from '../pages/discover/Discover';
import Menu from '../pages/menu/Menu';
import Contact from '../pages/contact/Contact';
import Book from '../pages/book/Book';
import { AnimatePresence } from "framer-motion";
import Gallery from "./carousel/Carousel"


function App() {
    return(
        <Router>
            <Navbar/>
            <AnimatePresence exitBeforeEnter>
                <Switch>        
                    <Route path="/" exact component={Home} />
                    <Route path="/discover" exact component={Discover} />
                    <Route path="/menu" exact component={Menu} />
                    <Route path="/contact" exact component={Contact} />
                    <Route path="/gallery" exact component={Gallery} />
                    <Route path="/book_table" exact component={Book} />
                </Switch>
            </AnimatePresence>
        </Router>
    );
}

export default App;