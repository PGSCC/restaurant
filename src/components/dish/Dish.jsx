import React from 'react';
import "./dish.css";

function Dish(props) {
    return(
        <div className="row Dish mt-4 pt-2">
            <div className="col-10">
                <h2>{props.foodName}</h2>
                <p>{props.option}</p>
            </div>
            <div className="col-2">
                <h2>{props.price}</h2>
            </div>
        </div>
    );
}

export default Dish;